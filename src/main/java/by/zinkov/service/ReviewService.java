package by.zinkov.service;

import by.zinkov.bean.Review;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllCountriesSpecification;
import by.zinkov.repository.impl.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class ReviewService {
    private RepositoryFactory repositoryFactory;

    @Autowired
    public ReviewService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }


    public Optional<Review> add(Review review) {
        Repository<Review> reviewRepository = repositoryFactory.getRepository(ReviewRepository.class);
        return reviewRepository.insert(review);
    }

    public void remove(Review review) {
        Repository<Review> reviewRepository = repositoryFactory.getRepository(ReviewRepository.class);
        reviewRepository.remove(review);
    }

    public void update(Review review) {
        Repository<Review> reviewRepository = repositoryFactory.getRepository(ReviewRepository.class);
        reviewRepository.update(review);
    }

    public Optional<Review> getByPk(int id) {
        Repository<Review> userRepository = repositoryFactory.getRepository(ReviewRepository.class);
        return userRepository.getByPK(id);
    }

    public List<Review> getAll() {
        Repository<Review> reviewRepository = repositoryFactory.getRepository(ReviewRepository.class);
        return reviewRepository.query(new AllCountriesSpecification());
    }
}
