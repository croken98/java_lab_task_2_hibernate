package by.zinkov.service;

import by.zinkov.bean.UserTour;
import by.zinkov.repository.Repository;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.AllCountriesSpecification;
import by.zinkov.repository.impl.UserTourRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class UserTourService {
    RepositoryFactory repositoryFactory;

    @Autowired
    public UserTourService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<UserTour> add(UserTour userTour) {
        Repository<UserTour> userTourRepository = repositoryFactory.getRepository(UserTourRepository.class);
        return userTourRepository.insert(userTour);
    }

    public void remove(UserTour userTour) {
        Repository<UserTour> userTourRepository = repositoryFactory.getRepository(UserTourRepository.class);
        userTourRepository.remove(userTour);
    }

    public void update(UserTour userTour) {
        Repository<UserTour> userTourRepository = repositoryFactory.getRepository(UserTourRepository.class);
        userTourRepository.update(userTour);
    }

    public Optional<UserTour> getByPk(int id) {
        Repository<UserTour> userRepository = repositoryFactory.getRepository(UserTourRepository.class);
        return userRepository.getByPK(id);
    }

    public List<UserTour> getAll() {
        Repository<UserTour> userTourRepository = repositoryFactory.getRepository(UserTourRepository.class);
        return userTourRepository.query(new AllCountriesSpecification());
    }
}
