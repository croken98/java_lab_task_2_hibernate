package by.zinkov.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan("by.zinkov.repository")
public class DBConfig {

    @Value("/hicaricp.properties")
    private String pathToProperty;

    @Bean
    public DataSource hikariDataSource() {
        HikariConfig config = new HikariConfig(pathToProperty);
        config.setMaximumPoolSize(20);
        return new HikariDataSource(config);
    }


    @Autowired
    @Bean
    public JdbcTemplate jdbcTemplate(@Qualifier("hikariDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}