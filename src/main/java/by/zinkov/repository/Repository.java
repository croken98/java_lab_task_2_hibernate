package by.zinkov.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
    Optional<T> insert(T object);

    void remove(T object);

    void update(T object);

    Optional<T> getByPK(int pk);
    List<T> query(Specification specification);
}
