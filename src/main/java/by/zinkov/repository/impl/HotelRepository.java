package by.zinkov.repository.impl;

import by.zinkov.bean.Features;
import by.zinkov.bean.Hotel;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

@Component("hotelRepository")
public class HotelRepository extends AbstractRepository<Hotel> {

    @Override
    protected void setterStatement(PreparedStatement statement, Hotel object) throws SQLException {
        statement.setString(1, object.getName());
        statement.setByte(2, object.getStars());
        statement.setString(3, object.getWebsite());
        statement.setString(4, object.getLalitude());
        statement.setString(5, object.getLongitude());
        Features[] featuresArray = object.getFeatures();
        Object[] featuresStringArray = Arrays.stream(featuresArray).map(Features::getName).toArray();
        Connection connection = statement.getConnection();
        Array array = connection.createArrayOf("features_enum", featuresStringArray);
        statement.setArray(6, array);
    }

    private void setterStatementForUpdate(PreparedStatement statement, Hotel object) throws SQLException {
        setterStatement(statement, object);
        statement.setInt(7, object.getId());

    }

    @Override
    protected String getSelectByPKQuery() {
        return "SELECT * FROM hotel WHERE id = ?";

    }


    @Override
    protected String getCreateQuery() {
        return "INSERT INTO hotel(name, stars , website, lalitude , longitude , features) VALUES(? , ? , ? , ? , ? , ? )";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(Hotel object) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void update(Hotel object) {
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(getUpdateQuery());
            setterStatementForUpdate(ps, object);
            return ps;
        });
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE hotel SET name = ?, stars = ?, website = ? , lalitude = ? , longitude = ? , features = ?   WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(Hotel object) {
        Object[] statementSetter = new Object[7];
        statementSetter[0] = object.getName();
        statementSetter[1] = object.getStars();
        statementSetter[2] = object.getWebsite();
        statementSetter[3] = object.getLalitude();
        statementSetter[4] = object.getLongitude();
        Features[] features = object.getFeatures();
        Object[] objectFeatures = Arrays.stream(features).map(Features::getName).toArray();
        statementSetter[5] = objectFeatures;
        statementSetter[6] = object.getId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM hotel WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(Hotel object) {
        Object[] statementSetter = new Object[1];
        statementSetter[0] = object.getId();
        return statementSetter;
    }
}
