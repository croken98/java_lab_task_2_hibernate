package by.zinkov.repository.impl;

import by.zinkov.bean.Country;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;



@Component("countryRepository")
public class CountryRepository extends AbstractRepository<Country> {

    @Override
    protected void setterStatement(PreparedStatement statement, Country object) throws SQLException {
        statement.setString(1,object.getName());
    }

    @Override
    protected String getSelectByPKQuery() {
        return "SELECT * FROM country WHERE id = ?";

    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO country(name) VALUES(?)";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(Country object) {
        Object[] statementSetter =  new Object[1];
        statementSetter[0] = object.getName();
        return statementSetter;
    }



    @Override
    protected String getUpdateQuery() {
        return "UPDATE country SET id = ?, name = ? WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(Country object) {
        Object[] statementSetter =  new Object[3];
        statementSetter[0] = object.getId();
        statementSetter[1] = object.getName();
        statementSetter[2] = object.getId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM country WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(Country object) {
        Object[] statementSetter =  new Object[1];
        statementSetter[0] = object.getId();
        return statementSetter;
    }

}
