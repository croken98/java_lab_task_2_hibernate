package by.zinkov.repository.impl;

import by.zinkov.bean.Tour;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component("tourRepository")
public class TourRepository extends AbstractRepository<Tour> {

    @Override
    protected void setterStatement(PreparedStatement statement, Tour object) throws SQLException {
        statement.setBytes(1,object.getPhoto());
        statement.setTimestamp(2,object.getDate());
        statement.setLong(3,object.getDuration());
        statement.setString(4,object.getDescription());
        statement.setBigDecimal(5,object.getCost());
        statement.setObject(6,object.getTour().getName(), java.sql.Types.OTHER);
        statement.setInt(7,object.getHotelId());
        statement.setInt(8,object.getCountryId());
    }


    private void setterStatementForUpdate(PreparedStatement statement, Tour object) throws SQLException {
        setterStatement(statement, object);
        statement.setInt(9,object.getId());

    }

    @Override
    protected String getSelectByPKQuery() {
        return "SELECT * FROM tour WHERE id = ?";

    }

    @Override
    protected String getCreateQuery() {
        return "INSERT INTO tour(photo, date , duration , description , cost, tour , hotel_id , country_id) VALUES(? , ? , ? , ? , ? , ? , ? , ? )";
    }

    @Override
    protected Object[] getPreparedStatementSetterForAdd(Tour object) {
        Object[] statementSetter =  new Object[8];
        statementSetter[0] = object.getPhoto();
        statementSetter[1] = object.getDate();
        statementSetter[2] = object.getDuration();
        statementSetter[3] = object.getDescription();
        statementSetter[4] = object.getCost();
        statementSetter[5] = object.getTour().getName();
        statementSetter[6] = object.getHotelId();
        statementSetter[7] = object.getCountryId();
        return statementSetter;
    }



    @Override
    protected String getUpdateQuery() {
        return "UPDATE tour SET photo = ?, date = ? , duration = ? , description = ? , cost = ? , tour=? , hotel_id = ? , country_id = ? WHERE id = ?;";
    }

    @Override
    public void update(Tour object) {
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(getUpdateQuery());
            setterStatementForUpdate(ps, object);
            return ps;
        });
    }

    @Override
    protected Object[] getPreparedStatementSetterForUpdate(Tour object) {
        Object[] statementSetter =  new Object[9];
        statementSetter[0] = object.getPhoto();
        statementSetter[1] = object.getDate();
        statementSetter[2] = object.getDuration();
        statementSetter[3] = object.getDescription();
        statementSetter[4] = object.getCost();
        statementSetter[5] = object.getTour().getName();
        statementSetter[6] = object.getHotelId();
        statementSetter[7] = object.getCountryId();
        statementSetter[8] = object.getId();
        return statementSetter;
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM tour WHERE id = ?;";
    }

    @Override
    protected Object[] getPreparedStatementSetterForDelete(Tour object) {
        Object[] statementSetter =  new Object[1];
        statementSetter[0] = object.getId();
        return statementSetter;
    }
}
