package by.zinkov.repository;

public interface Specification{

    String toSql();
    Object[] getArgs();

}
