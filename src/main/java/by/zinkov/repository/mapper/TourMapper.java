package by.zinkov.repository.mapper;

import by.zinkov.bean.Tour;
import by.zinkov.bean.TourType;
import org.springframework.stereotype.Component;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TourMapper implements RowMapper<Tour> {

    @Override
    public Tour mapRow(ResultSet rs, int rowNum) throws SQLException {
        Tour tour = new Tour();

        tour.setId(rs.getInt("id"));
        tour.setPhoto(rs.getBytes("photo"));
        tour.setDate(rs.getTimestamp("date"));
        tour.setDuration(rs.getLong("duration"));
        tour.setDescription(rs.getString("description"));
        tour.setCost(rs.getBigDecimal("cost"));
        TourType tourType = TourType.getByName(rs.getString("tour"));
        tour.setTour(tourType);
        tour.setHotelId(rs.getInt("hotel_id"));
        tour.setCountryId(rs.getInt("country_id"));

        return tour;
    }
}
