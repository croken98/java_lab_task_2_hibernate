package by.zinkov.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T> implements Repository<T> {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected RowMapper<T> rowMapper;

    protected abstract String getCreateQuery();

    protected abstract Object[] getPreparedStatementSetterForAdd(T object);

    protected abstract String getUpdateQuery();

    protected abstract Object[] getPreparedStatementSetterForUpdate(T object);


    protected abstract String getDeleteQuery();

    protected abstract Object[] getPreparedStatementSetterForDelete(T object);

    protected abstract String getSelectByPKQuery();

    protected abstract void setterStatement(PreparedStatement statement, T object) throws SQLException;

    @Override
    public Optional<T> getByPK(int pk) {
        List<T> objectList = jdbcTemplate.query(getSelectByPKQuery(), new Object[]{pk}, rowMapper);
        return objectList.isEmpty() ? Optional.empty() : Optional.of(objectList.get(0));
    }

    @Override
    public Optional<T> insert(T object) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(getCreateQuery(), new String[]{"id"});
            setterStatement(ps, object);
            return ps;
        }, keyHolder);


        return getByPK((Integer) keyHolder.getKey());
    }

    @Override
    public void remove(T object) {
        jdbcTemplate.update(getDeleteQuery(), getPreparedStatementSetterForDelete(object));
    }

    @Override
    public void update(T object) {
        jdbcTemplate.update(getUpdateQuery(), getPreparedStatementSetterForUpdate(object));
    }

    @Override
    public List<T> query(Specification specification) {
        return jdbcTemplate.query(specification.toSql(), specification.getArgs(), rowMapper);
    }
}
