CREATE TABLE user_travel_agency(
  id SERIAL PRIMARY KEY,
  login character varying(45)  not null,
  password character(32)  not null
);

CREATE TYPE features_enum as ENUM(
  'free wifi',
  'Wine Bar',
  'Billiards Table',
  'Indoor Heated Pool',
  'Fitness Center',
  'Rooftop Abseiling',
  'Business Services',
   'Guest Services',
   'Transportation & Parking',
   'Free drinks'
   );

CREATE TABLE user_tour(
  user_id INTEGER  not null,
  tour_id INTEGER  not null
  );

CREATE TABLE review(
  id SERIAL PRIMARY KEY  not null,
  date TIMESTAMP  not null,
  text TEXT  not null,
  userId INTEGER  not null,
  tourId INTEGER  not null
  );


CREATE TABLE country(
  id SERIAL PRIMARY KEY  not null,
  name CHARACTER VARYING(40)  not null
  );


CREATE TABLE hotel(
  id SERIAL PRIMARY KEY,
  name CHARACTER VARYING(45)  not null,
  stars smallint  not null,
  website character varying(100)  not null,
  lalitude character varying(25)  not null,
  longitude  character varying(25)  not null,
  features features_enum[]  not null
  );


CREATE TYPE tour_type_enum as ENUM(
  'Event Travel',
  'Visiting Friends or Relatives',
  'Business Travel',
  'The Gap Year',
  'Long Term Slow Travel',
  'Volunteer Travel',
  'The Caravan/RV Road Trip',
  'The Group Tour',
  'The Package Holiday',
  'The Weekend Break'
  );

CREATE TABLE tour(
  id SERIAL PRIMARY KEY,
  photo bytea  not null,
  date timestamp  not null,
  duration bigint  not null,
  description text  not null,
  cost decimal  not null,
  tour tour_type_enum  not null,
  hotel_id INTEGER  not null,
  country_id INTEGER  not null
  );


ALTER TABLE review
    ADD CONSTRAINT fk_reviews_user FOREIGN KEY (userId) REFERENCES user_travel_agency (id);

ALTER TABLE review
    ADD CONSTRAINT fk_reviews_tour FOREIGN KEY (tourId) REFERENCES tour(id);

ALTER TABLE user_tour
    ADD CONSTRAINT fk_user_tour_user FOREIGN KEY (user_id) REFERENCES user_travel_agency (id);


ALTER TABLE user_tour
    ADD CONSTRAINT fk_user_tour_tour FOREIGN KEY (tour_id) REFERENCES tour(id);

ALTER TABLE tour
    ADD CONSTRAINT fk_tour_country FOREIGN KEY (country_id) REFERENCES country(id);

ALTER TABLE tour
    ADD CONSTRAINT fk_tour_hotel FOREIGN KEY (hotel_id) REFERENCES hotel(id);