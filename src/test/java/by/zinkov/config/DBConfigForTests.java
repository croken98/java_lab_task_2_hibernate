package by.zinkov.config;


import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@ComponentScan("by.zinkov.repository")
public class DBConfigForTests {

    @Autowired
    EmbeddedPostgres testDatabase;

    @Bean(name = "embeddedDataSource")
    public DataSource mysqlDataSource() {
        return testDatabase.getPostgresDatabase();
    }

    @Bean
    public EmbeddedPostgres embeddedPostgres() throws IOException {
        return EmbeddedPostgres.start();
    }

    @Bean
    public Flyway flyway() {
        return Flyway.configure().dataSource(testDatabase.getPostgresDatabase()).envVars().load();
    }


    @Bean(name = "jdbcTemplateForTest")
    public JdbcTemplate jdbcTemplate(@Qualifier("embeddedDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}