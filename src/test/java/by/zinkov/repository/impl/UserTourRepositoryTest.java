package by.zinkov.repository.impl;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class UserTourRepositoryTest extends AbstractTest {
    @Autowired
    private UserTourRepository userTourRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TourRepository tourRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private AllUsersToursSpecification allUsersToursSpecification;


    @Test
    public void shouldReturnTwoUsersToursFromDB() {
        UserTour userTourOne = getDefaultUserTour();
        UserTour userTourTwo = getDefaultUserTour();
        userTourRepository.insert(userTourOne);
        userTourRepository.insert(userTourTwo);

        List<UserTour> userTours = userTourRepository.query(allUsersToursSpecification);
        Assert.assertEquals(2, userTours.size());
        Assert.assertEquals(userTourOne.getTourId(), userTours.get(0).getTourId());
        Assert.assertEquals(userTourTwo.getTourId(), userTours.get(1).getTourId());
    }

    @Test
    public void shouldAddUserTour() {
        UserTour userTour = getDefaultUserTour();
        Optional<UserTour> userTourOptional = userTourRepository.insert(userTour);

        UserTour expectedUserTour = getDefaultUserTour();
        expectedUserTour.setUserId(1);
        expectedUserTour.setTourId(1);

        Assert.assertEquals(expectedUserTour, userTourOptional.get());

    }

    @Test
    public void shouldAddAndRemoveObj() {
        UserTour userTour = getDefaultUserTour();
        Optional<UserTour> userTourOptional = userTourRepository.insert(userTour);
        userTourRepository.remove(userTourOptional.get());

        userTourOptional = userTourRepository.getByEntity(userTour);
        Assert.assertFalse(userTourOptional.isPresent());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<UserTour> userTour = userTourRepository.getByEntity(getDefaultUserTour());
        Assert.assertFalse(userTour.isPresent());
    }


    @Before
    public void initDB() {
        User user = new User();
        user.setLogin("login");
        user.setPassword("passpasspasspasspasspasspasspass");
        userRepository.insert(user);


        Country country = new Country();
        country.setName("RB");
        countryRepository.insert(country);

        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelRepository.insert(hotel);

        Tour tour = new Tour();
        tour.setCost(new BigDecimal(100));
        tour.setCountryId(1);
        tour.setDate(new Timestamp(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        tour.setHotelId(1);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);

        tourRepository.insert(tour);


    }

    private UserTour getDefaultUserTour() {
        UserTour userTour = new UserTour();
        userTour.setTourId(1);
        userTour.setUserId(1);
        return userTour;
    }

}