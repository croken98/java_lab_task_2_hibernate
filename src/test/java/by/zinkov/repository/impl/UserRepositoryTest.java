package by.zinkov.repository.impl;

import by.zinkov.bean.User;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class UserRepositoryTest extends AbstractTest {



    @Autowired
    UserRepository userRepository;

    @Autowired
    private AllUsersSpecification allUsersSpecification;

    @Test
    public void shouldAddUser() {
        User user = getDefaultUser();
        Optional<User> userOptional = userRepository.insert(user);

        User expectedUser = getDefaultUser();
        expectedUser.setId(1);

        Assert.assertEquals(expectedUser, userOptional.get());

    }



    @Test
    public void shouldReturnTwoUsersFromDB() {
        User userOne = getDefaultUser();
        User userTwo = getDefaultUser();
        userRepository.insert(userOne);
        userRepository.insert(userTwo);

        List<User> users = userRepository.query(allUsersSpecification);
        Assert.assertEquals(2, users.size());
        Assert.assertEquals(userOne.getLogin(), users.get(0).getLogin());
        Assert.assertEquals(userTwo.getLogin(), users.get(1).getLogin());
    }

    @Test
    public void shouldAddAndRemoveObj() {
        User user = getDefaultUser();
        Optional<User> userOptional = userRepository.insert(user);
        userRepository.remove(userOptional.get());

        userOptional = userRepository.getByPK(userOptional.get().getId());
        Assert.assertFalse(userOptional.isPresent());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<User> user = userRepository.getByPK(1);
        Assert.assertFalse(user.isPresent());
    }

    @Test
    public void shouldInsertUserAndUpdateUser() {
        User user = getDefaultUser();
        Optional<User> userOptional = userRepository.insert(user);
        User updatedUser = userOptional.get();
        updatedUser.setLogin("new login");
        userRepository.update(updatedUser);
        userOptional = userRepository.getByPK(updatedUser.getId());
        Assert.assertEquals("new login", userOptional.get().getLogin());
    }

    private User getDefaultUser() {
        User user = new User();
        user.setLogin("login");
        user.setPassword("passpasspasspasspasspasspasspass");
        return user;
    }


}