package by.zinkov.repository.impl;

import by.zinkov.bean.Features;
import by.zinkov.bean.Hotel;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class , MappersConfig.class})
public class HotelRepositoryTest extends AbstractTest{
    @Autowired
    HotelRepository hotelRepository;

    @Autowired
    AllHotelsSpecification allHotelsSpecification;

    private static final String LONGITUDE_TEST_STRING = "newLongitude";
    private static final String HOTEL_NAME = "Hotel";
    private static final String HOTEL_URL = "http//:hotel.com";
    private static final String LALITUDE = "12,23451";
    private static final String LONGITUDE = "234,23231";

    @Test
    public void shouldAddHotel(){
        Hotel hotel = getDefaultHotel();
        Optional<Hotel> hotelOptional = hotelRepository.insert(hotel);

        Hotel expectedHotel = getDefaultHotel();
        expectedHotel.setId(1);

        Assert.assertEquals(expectedHotel,hotelOptional.get());

    }

    @Test
    public void shouldAddAndRemoveObj() {
        Hotel hotel = getDefaultHotel();
        Optional<Hotel> hotelOptional = hotelRepository.insert(hotel);
        hotelRepository.remove(hotelOptional.get());

        hotelOptional = hotelRepository.getByPK(hotelOptional.get().getId());
        Assert.assertFalse(hotelOptional.isPresent());
    }



    @Test
    public void shouldReturnTwoHotelsFromDB(){
        Hotel hotelOne = getDefaultHotel();
        Hotel hotelTwo = getDefaultHotel();
        hotelRepository.insert(hotelOne);
        hotelRepository.insert(hotelTwo);

        List<Hotel> hotels = hotelRepository.query(allHotelsSpecification);
        Assert.assertEquals(2,hotels.size());
        Assert.assertEquals(hotelOne.getName(),hotels.get(0).getName());
        Assert.assertEquals(hotelTwo.getName(), hotels.get(1).getName());
    }


    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional(){
        Optional<Hotel> hotel = hotelRepository.getByPK(1);
        Assert.assertFalse(hotel.isPresent());
    }

    @Test
    public void shouldInsertUserAndUpdateUser(){
        Hotel hotel = getDefaultHotel();
        Optional<Hotel> hotelOptional = hotelRepository.insert(hotel);
        Hotel updatedHotel = hotelOptional.get();
        updatedHotel.setLongitude(LONGITUDE_TEST_STRING);
        hotelRepository.update(updatedHotel);
        hotelOptional = hotelRepository.getByPK(updatedHotel.getId());
        Assert.assertEquals(LONGITUDE_TEST_STRING, hotelOptional.get().getLongitude());
    }

    private Hotel getDefaultHotel(){
        Hotel hotel = new Hotel();
        hotel.setName(HOTEL_NAME);
        hotel.setStars((byte) 3);
        hotel.setWebsite(HOTEL_URL);
        hotel.setLalitude(LALITUDE);
        hotel.setLongitude(LONGITUDE);
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        return hotel;
    }


}