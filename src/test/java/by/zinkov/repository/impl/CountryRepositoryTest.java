package by.zinkov.repository.impl;

import by.zinkov.bean.Country;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class CountryRepositoryTest extends AbstractTest {

    private static final String RB = "RB";
    private static final String RU = "RU";

    @Autowired
    private AllCountriesSpecification allCountriesSpecification;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void shouldAddCountry() {
        Country country = getDefaultCountry();
        Optional<Country> countryOptional = countryRepository.insert(country);

        Country expectedCountry = getDefaultCountry();
        expectedCountry.setId(1);

        Assert.assertEquals(expectedCountry, countryOptional.get());

    }

    @Test
    public void shouldAddAndRemoveObj() {
        Country country = getDefaultCountry();
        Optional<Country> countryOptional = countryRepository.insert(country);
        countryRepository.remove(countryOptional.get());

        countryOptional = countryRepository.getByPK(countryOptional.get().getId());
        Assert.assertFalse(countryOptional.isPresent());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<Country> user = countryRepository.getByPK(1);
        Assert.assertFalse(user.isPresent());
    }

    @Test
    public void shouldReturnTwoCountriesFromDB() {
        Country countryOne = getDefaultCountry();
        Country countryTwo = getDefaultCountry();
        countryRepository.insert(countryOne);
        countryRepository.insert(countryTwo);

        List<Country> countries = countryRepository.query(allCountriesSpecification);
        Assert.assertEquals(2, countries.size());
        Assert.assertEquals(countryOne.getName(), countries.get(0).getName());
        Assert.assertEquals(countryTwo.getName(), countries.get(1).getName());
    }

    @Test
    public void shouldInsertCountryAndUpdateCountry() {
        Country country = getDefaultCountry();
        Optional<Country> countryOptional = countryRepository.insert(country);
        Country updatedCountry = countryOptional.get();
        updatedCountry.setName(RU);
        countryRepository.update(updatedCountry);
        countryOptional = countryRepository.getByPK(updatedCountry.getId());
        Assert.assertEquals(RU, countryOptional.get().getName());
    }

    private Country getDefaultCountry() {
        Country country = new Country();
        country.setName(RB);
        return country;
    }

}