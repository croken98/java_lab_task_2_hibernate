package by.zinkov.repository.impl;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class ReviewRepositoryTest extends AbstractTest {
    
    private static final String TEST_TEXT = "TEST_TEXT";
    private static final String TEST_LOGIN = "login";
    private static final String TEST_PASSWORD = "passpasspasspasspasspasspasspass";
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private TourRepository tourRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private AllReviewsSpecification allReviewsSpecification;


    @Test
    public void shouldAddUser() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.insert(review);

        Review expectedReview = getDefaultReview();
        expectedReview.setId(1);

        Assert.assertEquals(expectedReview, reviewOptional.get());

    }

    @Test
    public void shouldAddAndRemoveObj() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.insert(review);
        reviewRepository.remove(reviewOptional.get());

        reviewOptional = reviewRepository.getByPK(reviewOptional.get().getId());
        Assert.assertFalse(reviewOptional.isPresent());
    }


    @Test
    public void shouldReturnTwoReviewsFromDB() {
        Review reviewlOne = getDefaultReview();
        Review reviewTwo = getDefaultReview();
        reviewRepository.insert(reviewlOne);
        reviewRepository.insert(reviewTwo);

        List<Review> reviews = reviewRepository.query(allReviewsSpecification);
        Assert.assertEquals(2, reviews.size());
        Assert.assertEquals(reviewlOne.getText(), reviews.get(0).getText());
        Assert.assertEquals(reviewTwo.getText(), reviews.get(1).getText());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<Review> review = reviewRepository.getByPK(1);
        Assert.assertFalse(review.isPresent());
    }

    @Test
    public void shouldInsertUserAndUpdateUser() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.insert(review);
        Review updatedReview = reviewOptional.get();
        updatedReview.setText(TEST_TEXT);
        reviewRepository.update(updatedReview);
        reviewOptional = reviewRepository.getByPK(updatedReview.getId());
        Assert.assertEquals(TEST_TEXT, reviewOptional.get().getText());
    }


    @Before
    public void initDB() {
        User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setPassword("passpasspasspasspasspasspasspass");
        userRepository.insert(user);


        Country country = new Country();
        country.setName("RB");
        countryRepository.insert(country);

        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelRepository.insert(hotel);

        Tour tour = new Tour();
        tour.setCost(new BigDecimal(100));
        tour.setCountryId(1);
        tour.setDate(new Timestamp(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        tour.setHotelId(1);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);

        tourRepository.insert(tour);


    }

    private Review getDefaultReview() {
        Review review = new Review();
        review.setText("tour text");
        review.setTourId(1);
        review.setDate(new Timestamp(12423553));
        review.setUserId(1);
        return review;
    }
}