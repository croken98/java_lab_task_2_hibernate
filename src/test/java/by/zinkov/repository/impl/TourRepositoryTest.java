package by.zinkov.repository.impl;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.config.MappersConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class, MappersConfig.class})
public class TourRepositoryTest extends AbstractTest {
    @Autowired
    TourRepository tourRepository;

    @Autowired
    AllToursSpecification allToursSpecification;
    @Autowired
    HotelRepository hotelRepository;

    @Autowired
    CountryRepository countryRepository;


    @Test
    public void shouldReturnTwoToursFromDB() {
        Tour tourOne = getDefaultTour();
        Tour tourTwo = getDefaultTour();
        tourRepository.insert(tourOne);
        tourRepository.insert(tourTwo);

        List<Tour> tours = tourRepository.query(allToursSpecification);
        Assert.assertEquals(2, tours.size());
        Assert.assertEquals(tourOne.getDescription(), tours.get(0).getDescription());
        Assert.assertEquals(tourTwo.getDescription(), tours.get(1).getDescription());
    }


    @Test
    public void shouldAddTour() {
        Tour tour = getDefaultTour();
        Optional<Tour> tourOptional = tourRepository.insert(tour);
        Tour expectedTour = getDefaultTour();
        expectedTour.setId(1);
        Assert.assertEquals(expectedTour, tourOptional.get());

    }

    @Test
    public void shouldAddAndRemoveObj() {
        Tour tour = getDefaultTour();
        Optional<Tour> tourOptional = tourRepository.insert(tour);
        tourRepository.remove(tourOptional.get());

        tourOptional = tourRepository.getByPK(tourOptional.get().getId());
        Assert.assertFalse(tourOptional.isPresent());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<Tour> tour = tourRepository.getByPK(1);
        Assert.assertFalse(tour.isPresent());
    }

    @Test
    public void shouldInsertTourAndUpdateTour() {
        Tour tour = getDefaultTour();
        Optional<Tour> tourOptional = tourRepository.insert(tour);
        Tour updatedTour = tourOptional.get();
        updatedTour.setDescription("new descr");
        tourRepository.update(updatedTour);
        tourOptional = tourRepository.getByPK(updatedTour.getId());
        Assert.assertEquals("new descr", tourOptional.get().getDescription());
    }


    @Before
    public void initDb() {
        Country country = new Country();
        country.setName("RB");
        countryRepository.insert(country);

        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelRepository.insert(hotel);

    }


    private Tour getDefaultTour() {
        Tour tour = new Tour();
        tour.setCost(new BigDecimal(100));
        tour.setCountryId(1);
        tour.setDate(new Timestamp(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        tour.setHotelId(1);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);
        return tour;
    }

}