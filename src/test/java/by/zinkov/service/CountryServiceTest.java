package by.zinkov.service;


import by.zinkov.bean.Country;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.CountryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private CountryRepository countryRepositoryMock;

    @InjectMocks
    private CountryService countryService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(CountryRepository.class)).thenReturn(countryRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {

        Country countryOne = new Country();
        countryOne.setName("BY");

        Country countryTwo = new Country();
        countryTwo.setName("BY");
        countryTwo.setId(1);
        when(countryRepositoryMock.insert(countryOne)).thenReturn(Optional.of(countryTwo));
        Optional<Country> optionalCountry = countryService.add(countryOne);
        Mockito.verify(countryRepositoryMock, Mockito.times(1)).insert(countryOne);

        Assert.assertEquals(optionalCountry, Optional.of(countryTwo));

    }

    @Test
    public void removeTest() {
        Country country = new Country();
        country.setName("BY");
        country.setId(1);
        countryService.remove(country);
        Mockito.verify(countryRepositoryMock, Mockito.times(1)).remove(country);
    }

    @Test
    public void updateTest() {
        Country country = new Country();
        country.setName("BY");
        country.setId(1);
        countryService.update(country);
        Mockito.verify(countryRepositoryMock, Mockito.times(1)).update(country);
    }

    @Test
    public void whenCallGetByPKReturnOptional() {
        Country country = new Country();
        country.setName("BY");
        country.setId(1);
        when(countryRepositoryMock.getByPK(country.getId())).thenReturn(Optional.of(country));
        Optional<Country> optionalCountry = countryService.getByPk(country.getId());
        Mockito.verify(countryRepositoryMock, Mockito.times(1)).getByPK(country.getId());
        Assert.assertEquals(country, optionalCountry.get());
    }

}