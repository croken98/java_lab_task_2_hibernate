package by.zinkov.service;

import by.zinkov.bean.UserTour;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.UserTourRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserTourServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private UserTourRepository userTourRepositoryMock;

    @InjectMocks
    private UserTourService userTourService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(UserTourRepository.class)).thenReturn(userTourRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {
        UserTour userTourOne = new UserTour();
        userTourOne.setUserId(1);
        UserTour userTourTwo = new UserTour();
        userTourTwo.setUserId(1);
        userTourTwo.setUserId(1);
        when(userTourRepositoryMock.insert(userTourOne)).thenReturn(Optional.of(userTourTwo));
        Optional<UserTour> optionalUserTour = userTourService.add(userTourOne);
        Mockito.verify(userTourRepositoryMock, Mockito.times(1)).insert(userTourOne);
        Assert.assertEquals(optionalUserTour, Optional.of(userTourTwo));
    }

    @Test
    public void removeTest() {
        UserTour userTour = new UserTour();
        userTour.setUserId(1);
        userTour.setUserId(1);
        userTourService.remove(userTour);
        Mockito.verify(userTourRepositoryMock, Mockito.times(1)).remove(userTour);
    }

    @Test
    public void updateTest() {
        UserTour userTour = new UserTour();
        userTour.setUserId(1);
        userTour.setUserId(1);
        userTourService.update(userTour);
        Mockito.verify(userTourRepositoryMock, Mockito.times(1)).update(userTour);
    }

}